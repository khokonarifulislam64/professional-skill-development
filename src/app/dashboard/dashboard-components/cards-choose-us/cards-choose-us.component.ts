import { Component, OnInit } from '@angular/core';

interface cards {
  image: string;
  caption: string;
  breif: string;
}

@Component({
  selector: 'app-cards-choose-us',
  templateUrl: './cards-choose-us.component.html',
  styleUrls: ['./cards-choose-us.component.scss']
})
export class CardsChooseUsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  cards: cards [] = [
    {
      image: "assets/images/teacher.png",
      caption: "Expert Teacher",
      breif: "Learn from passionate industry experts committed to teaching.",
    },  
    {
      image: "assets/images/syllabus.png",
      caption: "Professional Syllabus",
      breif: "Lorem ipsum is placeholder text commonly used in the graphic",
    },
    {
      image: "assets/images/certification.png",
      caption: "Certification and Verification",
      breif: "Lorem ipsum is placeholder text commonly used in the graphic",
    },
    
  ]

}
