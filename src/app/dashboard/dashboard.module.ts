import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoFlexyModule } from '../demo-flexy-module'
import { DashboardComponent } from './dashboard.component';
import { CardsComponent } from './dashboard-components/cards/cards.component';
import { FormsModule } from '@angular/forms';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ComponentsElementModule } from '../component-elements/components-elements.module';
import { CardsChooseUsComponent } from './dashboard-components/cards-choose-us/cards-choose-us.component';




@NgModule({
  declarations: [
    DashboardComponent,
    CardsComponent,
    CardsChooseUsComponent
  ],
  imports: [
    CommonModule,
    DemoFlexyModule,
    FormsModule,
    NgApexchartsModule,
    ComponentsElementModule
  ],
  exports: [
    DashboardComponent
  ],
})
export class DashboardModule { }
