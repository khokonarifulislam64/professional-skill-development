import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseComponent } from './components/course/course.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FullComponent } from './layouts/full/full.component';
import { CheckCertificateComponent } from './components/check-certificate/check-certificate.component';
import { MyCourseComponent } from './components/my-course/my-course.component';
import { OurStudentComponent } from './components/our-student/our-student.component';
// import { OurTeacherComponent } from './components/our-teacher/our-teacher.component';
import { CourseSettingComponent } from './components/admin-course-setting/course-setting.component';
import { TeacherSettingComponent } from './components/admin-teacher-setting/teacher-setting.component';

const routes: Routes = [
  {
    path:"",
    component:FullComponent,
    children: [
      {path:"", redirectTo:"/home", pathMatch:"full"},
      {path:"home", component:DashboardComponent},
      {path:"course", component:CourseComponent},
      {path:"check-certificate", component:CheckCertificateComponent},
      {path:"my-course", component:MyCourseComponent},
      {path:"our-student", component:OurStudentComponent},
      // {path:"our-teacher", component:OurTeacherComponent},
      {path:"course-setting", component:CourseSettingComponent},
      {path:"teacher-setting", component:TeacherSettingComponent},
    ]
  },

  {path:"", redirectTo:"/home", pathMatch:"full"},
  {path:"**", redirectTo:"/home", pathMatch:"full"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
