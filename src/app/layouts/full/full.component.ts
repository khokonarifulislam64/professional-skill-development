import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

interface sidebarMenu {
  link: string;
  icon: string;
  menu: string;
}

@Component({
  selector: 'app-full',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent {

  search: boolean = false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) { }

  routerActive: string = "activelink";

  sidebarMenu: sidebarMenu[] = [
    {
      link: "/home",
      icon: "home",
      menu: "Home",
    },
    
    {
      link: "/course",
      icon: "layout",
      menu: "Course",
    },
    {
      link: "/my-course",
      icon: "info",
      menu: "My Course",
    },
    {
      link: "/our-student",
      icon: "smile",
      menu: "Our Student",
    },
    // {
    //   link: "/our-teacher",
    //   icon: "users",
    //   menu: "Our Teacher",
    // },
    {
      link: "/check-certificate",
      icon: "award",
      menu: "Check certificate",
    },
    {
      link: "/course-setting",
      icon: "file-plus",
      menu: "Course Setting",
    },
    {
      link: "/teacher-setting",
      icon: "user-plus",
      menu: "Teacher Setting",
    },
  ]

}
