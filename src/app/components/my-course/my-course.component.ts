import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DemoFlexyModule } from 'src/app/demo-flexy-module';

interface cards {
  image: string;
  btn: string;
}
@Component({
    selector: 'my-course',
    standalone: true,
    imports: [CommonModule,DemoFlexyModule],
    templateUrl: './my-course.component.html',
    styleUrls: ['./my-course.component.scss']
  })
export class MyCourseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  cards: cards [] = [
    {
      image: "assets/images/u2.webp",
      btn: "warn",
    },
    {
      image: "assets/images/u3.webp",
      btn: "primary",
    },
    {
      image: "assets/images/u4.webp",
      btn: "accent",
    },
  ]

}