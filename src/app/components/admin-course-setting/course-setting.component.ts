
import { Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { Router } from '@angular/router';
import { FeatherModule } from 'angular-feather';
import { CourseTableComponent } from './course-table/course-table.component';

@Component({
  selector: 'course-setting',
  standalone: true,
  imports: [CourseTableComponent,FeatherModule,MatFormFieldModule, MatInputModule, FormsModule, ReactiveFormsModule, MatCheckboxModule, MatRadioModule, MatButtonModule],
  templateUrl: './course-setting.component.html',
  styleUrls: ['./course-setting.component.scss']
})
export class  CourseSettingComponent implements OnInit {
  
  checked = true;

  fileInputControl = new FormControl();
  
  constructor(private _router: Router,) { }

  ngOnInit(): void {}

  onBack(): void {
    this._router.navigate(['/flexy/home']);
  }

  //file upload test
  fileName = '';

  onFileSelected(event:any) {

      const file:File = event.target.files[0];

      if (file) {

          this.fileName = file.name;
          console.log(this.fileName)

          const formData = new FormData();

          formData.append("thumbnail", file);

        //  const upload$ = this.http.post("/api/thumbnail-upload", formData);

         // upload$.subscribe();
      }
  }
}