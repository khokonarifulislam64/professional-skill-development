import { Component, OnInit } from '@angular/core';
import { DashboardModule } from "../../dashboard/dashboard.module";


@Component({
    selector: 'check-certificate',
    standalone: true,
    templateUrl: './check-certificate.component.html',
    imports: [DashboardModule]
})
export class CheckCertificateComponent implements OnInit {

    constructor() { }
  
    ngOnInit(): void {
    }
}  