import { CourseTableComponent } from './admin-course-setting/course-table/course-table.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { DemoFlexyModule } from '../demo-flexy-module';
import { FormsModule } from '@angular/forms';
import { CourseComponent } from './course/course.component';
import { CheckCertificateComponent } from './check-certificate/check-certificate.component';
import { MyCourseComponent } from './my-course/my-course.component';
import { OurStudentComponent } from './our-student/our-student.component';
import { OurTeacherComponent } from './our-teacher/our-teacher.component';
import { CourseSettingComponent } from './admin-course-setting/course-setting.component';
import { TeacherSettingComponent } from './admin-teacher-setting/teacher-setting.component';

@NgModule({
  imports: [
    CommonModule,
    FeatherModule.pick(allIcons),
    DemoFlexyModule,
    FormsModule,
    CourseComponent,
    CourseTableComponent,
    CheckCertificateComponent,
    MyCourseComponent,
    OurStudentComponent,
    OurTeacherComponent,
    CourseSettingComponent,
    TeacherSettingComponent
  ],
  exports: [
  ]
})
export class ComponentsModule { }
