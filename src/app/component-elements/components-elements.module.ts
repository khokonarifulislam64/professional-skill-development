import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadlineComponent } from './headline/headline.component';
import { DetailsParagraph } from './details-paragraph/details-paragraph.component';
import { SliderComponent } from './slider/slider.component';
import { DemoFlexyModule } from '../demo-flexy-module'
import { CarouselModule } from 'ngx-owl-carousel-o';

@NgModule({
  declarations: [
    HeadlineComponent,
    DetailsParagraph,
    SliderComponent,
    
  ],
  imports: [
    CommonModule,
    DemoFlexyModule,
    CarouselModule,
  ],
  exports: [
    HeadlineComponent,
    DetailsParagraph,
    SliderComponent
  ]
})

export class ComponentsElementModule { }
