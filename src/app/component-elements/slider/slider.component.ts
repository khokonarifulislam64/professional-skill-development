import { Component } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { FaConfig } from '@fortawesome/angular-fontawesome';


interface cards {
  id:string,
  image: string;
  btn: string;
}

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent {
  faCoffee = FaConfig;

  cards: cards [] = [
    {
      id:'1',
      image: "assets/images/u2.webp",
      btn: "warn",
    },
    {
      id:'2',
      image: "assets/images/u3.webp",
      btn: "primary",
    },
    {
      id:'3',
      image: "assets/images/u4.webp",
      btn: "accent",
    },
    {
      id:'4',
      image: "assets/images/u3.webp",
      btn: "primary",
    },
    {
      id:'5',
      image: "assets/images/u4.webp",
      btn: "accent",
    },
  ]

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }
}
