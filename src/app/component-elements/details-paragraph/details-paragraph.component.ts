import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-details-paragraph',
  templateUrl: './details-paragraph.component.html',
  styleUrls: ['./details-paragraph.component.scss']
})
export class DetailsParagraph {
  @Input() para: string = 'Default Title';
}
