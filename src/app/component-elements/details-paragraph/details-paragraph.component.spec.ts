import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsParagraph } from './details-paragraph.component';

describe('HeadlineComponent', () => {
  let component: DetailsParagraph;
  let fixture: ComponentFixture<DetailsParagraph>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsParagraph]
    });
    fixture = TestBed.createComponent(DetailsParagraph);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
